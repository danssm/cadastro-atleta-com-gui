package Model;

public class JogKarate extends Atleta{
	
	private String graduacao;
	private String golpeForte; 
	
	
	public String getGraduacao() {
		return graduacao;
	}


	public void setGraduacao(String graduacao) {
		this.graduacao = graduacao;
	}


	public String getGolpeForte() {
		return golpeForte;
	}


	public void setGolpeForte(String golpeForte) {
		this.golpeForte = golpeForte;
	}


	public JogKarate(String nome, String graduacao) {
		super(nome);
		this.graduacao = graduacao;
	}
	
	

}
