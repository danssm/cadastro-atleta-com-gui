package Model;

public class JogVoley extends Atleta{
	
	private double altura;
	private int nivelAgilidade; // 0 a 10 

	public JogVoley(String nome, double altura) {
		super(nome);
		this.altura = altura;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public int getNivelAgilidade() {
		return nivelAgilidade;
	}

	public void setNivelAgilidade(int nivelAgilidade) {
		this.nivelAgilidade = nivelAgilidade;
	}

}
